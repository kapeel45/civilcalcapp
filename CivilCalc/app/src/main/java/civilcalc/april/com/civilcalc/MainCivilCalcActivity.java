package civilcalc.april.com.civilcalc;

import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

import static android.R.id.text1;

public class MainCivilCalcActivity extends AppCompatActivity {

    double plotArea,roadWidening,grossPlot,reservation,slumCalc;

    EditText editPlotArea;
    EditText editRoadWidening, editReservation;
    EditText editGrossPlot, editTdr,editDr;
    private RadioGroup radioPercent;
    private RadioButton percent;
    TextView textPermisiableTDR,textSlumCalc;
    double calcTDRPercent,grossPlotValue,drValue,tdrPermValue = 0;

    View v;
    private static final String TAG = "MyActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_civil);


        radioPercent = (RadioGroup) findViewById(R.id.radioPercent);

        editRoadWidening = (EditText)findViewById(R.id.edit_road_wide);
        editGrossPlot = (EditText)findViewById(R.id.edit_gross_plot);
        editPlotArea = (EditText)findViewById(R.id.edit_plot_area);
        editReservation = (EditText)findViewById(R.id.edit_reservation);

        editGrossPlot.setEnabled(false);
        editDr = (EditText)findViewById(R.id.edit_dr);


        editRoadWidening.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String strRoadWidening = editRoadWidening.getText().toString();

                if(strRoadWidening.isEmpty()){
                    strRoadWidening = "0";
                }

                plotArea = Double.parseDouble(editPlotArea.getText().toString());
                roadWidening = Double.parseDouble(strRoadWidening);

                editGrossPlot.setText(plotArea-roadWidening+"");
            }

        });

        editReservation.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String strReservation = editReservation.getText().toString();
                String strRoadWidening = editRoadWidening.getText().toString();

                plotArea = Double.parseDouble(editPlotArea.getText().toString());

                if(strRoadWidening.isEmpty()){
                    strRoadWidening = "0";
                }

                if (strReservation.isEmpty()){
                    strReservation = "0";
                }

                roadWidening = Double.parseDouble(strRoadWidening);


                reservation = Double.parseDouble(strReservation);

                editGrossPlot.setText(plotArea-reservation-roadWidening+"");
            }

        });

        radioPercent.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                int selectedPercent = radioPercent.getCheckedRadioButtonId();

                percent = (RadioButton) findViewById(selectedPercent);

                String textPercent = percent.getText().toString();
                Log.v(TAG,textPercent);
                editTdr = (EditText)findViewById(R.id.edit_tdr);
                editTdr.setEnabled(false);

                textPermisiableTDR =  (TextView) findViewById(R.id.perm_tdr_value);
                textSlumCalc = (TextView) findViewById(R.id.slum_calc);

                DecimalFormat round = new DecimalFormat ("###.####");

                switch (textPercent)
                {
                    case "9mt. and above but less than 12 (0.40%)":

                        Log.v("TAG","Selected radio Id: "+percent.getText());
                        grossPlotValue = Double.parseDouble(editGrossPlot.getText().toString());
                        Log.v("TAG","Gross Plot Value: "+grossPlotValue);
                        calcTDRPercent = (grossPlotValue -  (grossPlotValue/100) * 0.40);
                        calcTDRPercent = calcTDRPercent - (calcTDRPercent/100) * 20;
                        Log.v("TAG","TDR Calculated 9mt: "+calcTDRPercent);

                        //textSlumCalc.setText("20% Slum: "+round.format(calcTDRPercent));

                        editTdr.setText(round.format(calcTDRPercent)+"");
                        drValue = Double.parseDouble(editDr.getText().toString());

                        tdrPermValue = calcTDRPercent - drValue;

                        textPermisiableTDR.setText(round.format(tdrPermValue));

                        break;
                    case "12mt. and above but less than 18 (0.65%)":

                        Log.v("TAG","Selected radio Id: "+percent.getText());
                        grossPlotValue = Double.parseDouble(editGrossPlot.getText().toString());
                        Log.v("TAG","Gross Plot Value: "+grossPlotValue);
                        calcTDRPercent = grossPlotValue - (grossPlotValue/100) * 0.65;
                        calcTDRPercent = calcTDRPercent - (calcTDRPercent / 100) * 20;
                        Log.v("TAG","TDR Calculated 12mt: "+calcTDRPercent);
                        //textSlumCalc.setText("20% Slum: "+round.format(calcTDRPercent));

                        editTdr.setText(round.format(calcTDRPercent)+"");

                        drValue = Double.parseDouble(editDr.getText().toString());

                        tdrPermValue = calcTDRPercent - drValue;

                        textPermisiableTDR.setText(round.format(tdrPermValue));

                        break;
                    case "18mt. and above but less than 24 (0.90%)":
                        Log.v("TAG","Selected radio Id: "+percent.getText());
                        grossPlotValue = Double.parseDouble(editGrossPlot.getText().toString());
                        Log.v("TAG","Gross Plot Value: "+grossPlotValue);
                        calcTDRPercent = grossPlotValue - (grossPlotValue / 100) * 0.90;
                        calcTDRPercent = calcTDRPercent - (calcTDRPercent/100) * 20;
                        Log.v("TAG","TDR Calculated 18mt: "+calcTDRPercent);
                        //textSlumCalc.setText("20% Slum: "+round.format(calcTDRPercent));

                        editTdr.setText(round.format(calcTDRPercent)+"");
                        drValue = Double.parseDouble(editDr.getText().toString());

                        tdrPermValue = calcTDRPercent - drValue;

                        textPermisiableTDR.setText(round.format(tdrPermValue));

                        break;
                    case "24mt. and above but less than 30 (1.15%)":
                        Log.v("TAG","Selected radio Id: "+percent.getText());
                        grossPlotValue = Double.parseDouble(editGrossPlot.getText().toString());
                        Log.v("TAG","Gross Plot Value: "+grossPlotValue);
                        calcTDRPercent = grossPlotValue  - (grossPlotValue / 100) * 1.15;
                        calcTDRPercent = calcTDRPercent - (calcTDRPercent/100) * 20;
                        Log.v("TAG","TDR Calculated for 24mt: "+calcTDRPercent);
                        //textSlumCalc.setText("20% Slum: "+round.format(slumCalc));

                        editTdr.setText(round.format(calcTDRPercent)+"");
                        drValue = Double.parseDouble(editDr.getText().toString());

                        tdrPermValue = calcTDRPercent - drValue;

                        textPermisiableTDR.setText(round.format(tdrPermValue));
                        break;
                    case "30mt. and above (1.40%)":
                        Log.v("TAG","Selected radio Id: "+percent.getText());
                        grossPlotValue = Double.parseDouble(editGrossPlot.getText().toString());
                        Log.v("TAG","Gross Plot Value: "+grossPlotValue);
                        calcTDRPercent = grossPlotValue - (grossPlotValue / 100) * 1.40;
                        calcTDRPercent = calcTDRPercent - (calcTDRPercent/100) * 20;
                        Log.v("TAG","TDR Calculated for 30mt: "+calcTDRPercent);
                        //textSlumCalc.setText("20% Slum: "+round.format(slumCalc));

                        editTdr.setText(round.format(calcTDRPercent)+"");
                        drValue = Double.parseDouble(editDr.getText().toString());

                        tdrPermValue = calcTDRPercent - drValue;

                        textPermisiableTDR.setText(round.format(tdrPermValue));
                        break;
                }
            }
        });

        editDr.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                double drValue, calcTDR = 0;

                String strDr = editDr.getText().toString();
                DecimalFormat round = new DecimalFormat ("###.####");
                calcTDR = Double.parseDouble(editTdr.getText().toString());

                if(strDr.isEmpty()){
                    strDr = "0";
                }

                drValue = Double.parseDouble(strDr);

                tdrPermValue = calcTDR - drValue;

                textPermisiableTDR.setText(round.format(tdrPermValue));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        /*calButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Log.v(TAG, "on click called");




                Log.v(TAG, "percent is : "+plotArea);

                grossPlot = plotArea - roadWidening - reservation;
                Log.v(TAG, "Gross Plot: " + grossPlot + "");
                editGrossPlot.setText("" + grossPlot);
                Log.v(TAG, "calculating");


                Toast.makeText(getApplicationContext(),"Gross Plot = "+grossPlot,Toast.LENGTH_LONG);

            }
        });
*/
    }



}
